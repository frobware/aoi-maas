#!/bin/bash -e

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source $SCRIPT_DIR/config.sh

echo "Importing boot resources..."
aoi-ssh $INSTANCE maas $PROFILE boot-resources import

function get_boot_resource_id {
    while [ 1 ]; do
        BOOT_RESOURCE_ID=$(aoi-ssh $INSTANCE "maas $PROFILE boot-resources read | jq '.[0].id'")
        if [ "$BOOT_RESOURCE_ID" != "null" -o "$BOOT_RESOURCE_ID" == "" ]; then
            echo "$BOOT_RESOURCE_ID"
            return
        fi
        sleep 0.5
    done
}

echo ""
echo ""
echo "Waiting for region to import boot resources..."
BOOT_RESOURCE_ID=$(get_boot_resource_id)
aoi-ssh $INSTANCE "while [ \"\$(maas $PROFILE boot-resource read $BOOT_RESOURCE_ID | jq '.[] | .[]? | .complete')\" != \"true\" ]; do sleep 0.5; done"

# Shortcut: The rack ID is the same as the region ID for the first rack.
# So just read the rack ID from the file.
function get_rack_id {
    aoi-ssh $INSTANCE sudo cat /var/lib/maas/maas_id
}

echo ""
echo "Waiting for rack to import boot resources..."
RACK_ID=$(get_rack_id)
aoi-ssh $INSTANCE "while [ \"\$(maas $PROFILE rack-controller list-boot-images $RACK_ID | jq --raw-output '.status' 2> /dev/null)\" != \"synced\" ]; do sleep 0.5; done"
